# Movies

Simple API for fetching and saving movies from IMDB and commenting existing movies.

## Chosen db and libs:
**POSTGRES:** 

* Most commonly used DB
* DENSE_RANK Window function for ranking

**REQUESTS**

* Making requests to third-party apis (IMDB)

**DJANGO-REST-FRAMEWORK**

* Most popular and well-documented toolkit for creating WEB APIs
* Needed functionality out of the box
* There is no reason not to use this

**DJANGO-FILTER**

* Automated filtering and sorting querysets based on query params
* Easy DRF Integration

## Running

###Traditional

* Set-up postgres database
* Make DATABASES changes in settings.py
```
pip install -r requirements.txt
python manage.py makemigrations core
python manage.py migrate
python manage.py runserver 80
```

###Using Docker

```
git clone https://hidebehind3@bitbucket.org/hidebehind3/movies.git
cd movies
docker-compose up -d --build
```

## Using

### Movies
* List of all movies
    * GET /movies/
* Ordering movies by any its field
    * GET /movies/?ordering=year
    * GET /movies/?ordering=-year
* Filtering movies by any its field
    * GET /movies/?rated=PG
* Add new movie
    * POST /movies/
    * body:
    ```
    {"title": "Home Alone"}
    ```

### Comments 
* List of all comments
    * GET /comments/
* Filtering comments by movie ID
    * GET /comments/?movie=1
* Add new comment
    * POST /comments/
    * body:
    ```
    {"text": "Comment!", "movie": 1}
    ```
    
### Top
* List of top movies based on number of comments. Date range is required.
    * GET /top/?released_before=2019-01-01&released_after=1965-01-01

## Testing

###Traditional

```
python manage.py test
```

###Using Docker

```
docker-compose run web python manage.py test
```