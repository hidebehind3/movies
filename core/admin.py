from django.contrib import admin

from .models import Movie, Comment


admin.site.register(Movie, admin.ModelAdmin)
admin.site.register(Comment, admin.ModelAdmin)
