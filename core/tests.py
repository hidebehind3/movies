from django.urls import reverse

from rest_framework import status
from rest_framework.test import APITestCase

from .models import Movie, Comment


class MovieTestBase(APITestCase):

    def setUp(self):
        self.movies_url = reverse('movies')
        self.comments_url = reverse('comments')
        self.top_url = reverse('top')

    def create_movie(self, title):
        data = {'title': title}
        response = self.client.post(self.movies_url, data, format='json')
        return response

    def create_batch_movies(self):
        titles = ['Home Alone', 'Star Wars', 'The Godfather', 'The Dark Knight', 'Pulp Fiction']
        for title in titles:
            self.create_movie(title)


class MovieTest(MovieTestBase):

    def test_movies_endpoint(self):
        """
        Test /movies Endpoint.
        """
        response = self.client.get(self.movies_url)
        self.assertEqual(response.status_code, status.HTTP_200_OK)

    def test_movie_created(self):
        """
        Test to verify the Movie is created.
        """
        response = self.create_movie('Home Alone')
        self.assertEqual(response.status_code, status.HTTP_201_CREATED)
        self.assertEqual(Movie.objects.count(), 1)
        self.assertEqual(Movie.objects.get().title, 'Home Alone')

    def test_movie_not_created_twice(self):
        """
        Test to verify we are not creating the same movie twice.
        """
        self.create_movie('Home Alone')
        response = self.create_movie('Home Alone')
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertEqual(Movie.objects.count(), 1)
        self.assertEqual(Movie.objects.get().title, 'Home Alone')

    def test_movie_filtering(self):
        """
        Test movies filtering by rated field
        """
        self.create_batch_movies()
        response = self.client.get(self.movies_url, {'rated': 'PG'})
        self.assertEqual(len(response.json()), 2)


class CommentTest(MovieTestBase):

    def test_comments_endpoint(self):
        """
        Test /comments endpoint.
        """
        response = self.client.get(self.comments_url)
        self.assertEqual(response.status_code, status.HTTP_200_OK)

    def test_movie_id_not_provided(self):
        """
        Test to verify comment not created. No movie ID provided.
        """
        response = self.client.post(self.comments_url, {'text': 'Comment!'}, format='json')
        self.assertEqual(response.status_code, status.HTTP_400_BAD_REQUEST)
        self.assertEqual(Comment.objects.all().count(), 0)

    def test_text_not_provided(self):
        """
        Test to verify comment not created. No comment text provided.
        """
        response = self.client.post(self.comments_url, {'movie': 1}, format='json')
        self.assertEqual(response.status_code, status.HTTP_400_BAD_REQUEST)
        self.assertEqual(Comment.objects.all().count(), 0)

    def test_comment_created(self):
        """
        Test to verify the comment is created for provided movie.
        """
        self.create_movie('The Godfather')
        movie = Movie.objects.get()
        response = self.client.post(self.comments_url, {'text': 'Comment!', 'movie': movie.pk}, format='json')
        self.assertEqual(response.status_code, status.HTTP_201_CREATED)
        self.assertEqual(Comment.objects.all().count(), 1)
        self.assertEqual(Comment.objects.get().text, 'Comment!')
        self.assertEqual(Comment.objects.get().movie, movie)

    def test_comment_filtering(self):
        """
        Test comments filtering by movie ID
        """
        self.create_movie('The Godfather')
        self.create_movie('The Dark Knight')
        self.client.post(self.comments_url, {'text': 'Comment 1!', 'movie': Movie.objects.first().pk}, format='json')
        self.client.post(self.comments_url, {'text': 'Comment 2!', 'movie': Movie.objects.first().pk}, format='json')
        self.client.post(self.comments_url, {'text': 'Comment 3!', 'movie': Movie.objects.last().pk}, format='json')
        response = self.client.get(self.comments_url, {'movie': Movie.objects.first().pk})
        self.assertEqual(len(response.json()), 2)


class TestTop(MovieTestBase):

    def setUp(self):
        """
        For each movie create different num of comments.
        """
        super().setUp()
        self.create_batch_movies()
        for i, movie in enumerate(Movie.objects.all(), 1):
            for _ in range(i):
                Comment.objects.create(text='Lorem', movie=movie)

    def test_top_no_date(self):
        """
        Test endpoint without date range provided.
        """
        response = self.client.get(self.top_url)
        self.assertEqual(response.status_code, status.HTTP_400_BAD_REQUEST)

    def test_range(self):
        """
        Test correct number of objects returned.
        Home Alone - 1995
        Star Wars - 1977
        The Godfather - 1972
        The Dark Knight - 2008
        Pulp Fiction - 1994
        """
        response = self.client.get(self.top_url, {'released_before': '1995-01-01',
                                                  'released_after': '1985-01-01'})
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertEqual(len(response.json()), 2)  # Home Alone, Pulp Fiction

    def test_top(self):
        """
        Test ranking.
        """
        response = self.client.get(self.top_url, {'released_before': '1995-01-01',
                                                  'released_after': '1985-01-01'})
        resp_json = response.json()
        self.assertEqual(resp_json[0]['rank'], 1)  # 5 comments
        self.assertEqual(resp_json[1]['rank'], 2)  # 1 Comment

    def test_same_rank(self):
        """
        Test Ranking. Movies with same comments count should have same rank.
        """
        movie = Movie.objects.get(title='The Dark Knight')
        Comment.objects.create(text='Lorem', movie=movie)
        response = self.client.get(self.top_url, {'released_before': '2019-01-01',
                                                  'released_after': '1960-01-01'})
        resp_json = response.json()
        self.assertEqual(resp_json[0]['rank'], resp_json[1]['rank'], 1)
        self.assertEqual(resp_json[2]['rank'], 2)
        self.assertEqual(resp_json[3]['rank'], 3)
        self.assertEqual(resp_json[4]['rank'], 4)
