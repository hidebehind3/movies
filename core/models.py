from django.db import models


class Movie(models.Model):
    title = models.CharField(max_length=400)
    year = models.CharField(blank=True, max_length=50)  # Year can be in format "2013–"
    rated = models.CharField(max_length=200, blank=True)
    released = models.DateField(blank=True, null=True)
    runtime = models.CharField(max_length=200, blank=True)
    genre = models.CharField(max_length=200, blank=True)
    director = models.CharField(max_length=200, blank=True)
    writer = models.TextField(blank=True)
    actors = models.TextField(blank=True)
    plot = models.TextField(blank=True)
    language = models.CharField(max_length=200, blank=True)
    country = models.CharField(max_length=200, blank=True)
    awards = models.CharField(max_length=200, blank=True)
    poster = models.URLField(blank=True)
    metascore = models.SmallIntegerField(blank=True, null=True)
    imdb_rating = models.FloatField(blank=True, null=True)
    imdb_votes = models.IntegerField(blank=True, null=True)
    imdb_id = models.CharField(max_length=100, blank=True)
    type = models.CharField(max_length=100, blank=True)
    dvd = models.DateField(blank=True, null=True)
    box_office = models.CharField(max_length=200, blank=True)
    production = models.CharField(max_length=200, blank=True)
    website = models.URLField(blank=True)

    class Meta:
        ordering = ('id',)

    def __str__(self):
        return self.title


class Comment(models.Model):
    movie = models.ForeignKey(Movie, on_delete=models.CASCADE, related_name='comments')
    text = models.TextField()

    class Meta:
        ordering = ('id',)

    def __str__(self):
        return self.text[:30]
