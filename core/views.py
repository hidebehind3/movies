from datetime import datetime
import requests

from django.db.models import F, Count
from django.db.models.expressions import Window
from django.db.models.functions import DenseRank
from django.http import JsonResponse
from django.core.validators import URLValidator
from django.core.exceptions import ValidationError

from rest_framework.generics import ListCreateAPIView, ListAPIView
from rest_framework.filters import OrderingFilter
from django_filters.rest_framework import DjangoFilterBackend, DateFromToRangeFilter, FilterSet

from Movies.keys import IMDB_API_ID, IMDB_API_KEY
from .models import Movie, Comment
from .serializers import MovieSerializer, MovieTopSerializer, CommentSerializer


def parse_imdb_date(date_str):
    try:
        return datetime.strptime(date_str, '%d %b %Y').date()
    except Exception:
        return None


def parse_imdb_int(int_str):
    try:
        return int(int_str.replace(',', ''))
    except Exception:
        return None


def parse_imdb_url(url_str):
    validate = URLValidator()
    try:
        validate(url_str)
    except Exception:
        return ''
    else:
        return url_str


def imdb_object_adapter(imdb_obj):
    """
    Sometimes IMDB API returns weird results
    """
    return dict(title=imdb_obj.get('Title', ''),
                year=imdb_obj.get('Year', ''),
                rated=imdb_obj.get('Rated', ''),
                released=parse_imdb_date(imdb_obj.get('Released')),
                runtime=imdb_obj.get('Runtime', ''),
                genre=imdb_obj.get('Genre', ''),
                director=imdb_obj.get('Director', ''),
                writer=imdb_obj.get('Writer', ''),
                actors=imdb_obj.get('Actors', ''),
                plot=imdb_obj.get('Plot', ''),
                language=imdb_obj.get('Language', ''),
                country=imdb_obj.get('Country', ''),
                awards=imdb_obj.get('Awards', ''),
                poster=parse_imdb_url(imdb_obj.get('Poster')),
                metascore=parse_imdb_int(imdb_obj.get('Metascore')),
                imdb_rating=imdb_obj.get('imdbRating'),
                imdb_votes=parse_imdb_int(imdb_obj.get('imdbVotes')),
                imdb_id=imdb_obj.get('imdbID', ''),
                type=imdb_obj.get('Type', ''),
                dvd=parse_imdb_date(imdb_obj.get('DVD')),
                box_office=imdb_obj.get('BoxOffice', ''),
                production=imdb_obj.get('Production', ''),
                website=parse_imdb_url(imdb_obj.get('Website')))


class MoviesView(ListCreateAPIView):
    serializer_class = MovieSerializer
    queryset = Movie.objects.all()
    filter_backends = (OrderingFilter, DjangoFilterBackend)
    ordering_fields = '__all__'
    filter_fields = '__all__'

    def post(self, request, *args, **kwargs):
        try:
            response = requests.get('http://www.omdbapi.com/', params={'i': IMDB_API_ID,
                                                                       'apikey': IMDB_API_KEY,
                                                                       't': request.data.get('title')})
        except requests.exceptions.RequestException:
            return JsonResponse({'message': 'IMDB API Connection Error'}, status=400)
        else:
            response_json = response.json()
            movie_found_in_imdb = response_json.get('Response') == 'True'
            if movie_found_in_imdb:
                try:
                    movie = Movie.objects.get(imdb_id=response_json['imdbID'])
                except Movie.DoesNotExist:
                    serialized = MovieSerializer(data=imdb_object_adapter(response_json))
                    if serialized.is_valid():
                        serialized.save()
                        return JsonResponse(serialized.data, status=201)
                    else:
                        return JsonResponse(serialized.errors, status=400)
                else:
                    return JsonResponse(MovieSerializer(movie).data)
            return JsonResponse({'message': 'Movie not found.'}, status=404)


class CommentsView(ListCreateAPIView):
    serializer_class = CommentSerializer
    queryset = Comment.objects.all()
    filter_backends = (DjangoFilterBackend,)
    filter_fields = ('movie',)


class MovieDateFilterSet(FilterSet):
    released = DateFromToRangeFilter(required=True)

    class Meta:
        model = Movie
        fields = ('released',)


class TopMoviesListView(ListAPIView):
    queryset = Movie.objects.all()
    serializer_class = MovieTopSerializer
    filter_backends = (DjangoFilterBackend,)
    filterset_class = MovieDateFilterSet

    def get_queryset(self):
        return self.queryset.annotate(
            total_comments=Count('comments'),
            rank=Window(
                expression=DenseRank(),
                order_by=F('total_comments').desc()
            ),
        ).order_by('rank')
